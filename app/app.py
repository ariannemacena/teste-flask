from flask import Flask, render_template

app = Flask(__name__)
 
@app.route('/')
def home():
    variables = {
        'carros': {
            'label': 'Carros',
            'data': [2012, 5473, 4783, 3599, 3081, 2759, 3745],
            'color': 'rgb(77 83 253)',
            'colorH': 'rgb(77 83 253 / 60%)',
            'dataHour': [2463, 2973, 4264, 5282, 5279, 5316, 4155, 3734, 4844, 3984, 4094, 2258, 5550, 2239, 3720, 3303, 5495, 3505, 2292, 3754, 3192, 2629, 2728, 2917]
        },
        'buzes': {
            'label': 'Ônibus',
            'data': [500, 573, 483, 399, 381, 259, 345],
            'color': 'rgb(145 77 253)',
            'colorH': 'rgb(145 77 253 / 60%)',
            'dataHour': [399, 280, 433, 441, 277, 563, 531, 509, 223, 478, 543, 206, 585, 329, 238, 475, 364, 282, 515, 545, 388, 321, 497, 285]
        },
        'bikes': {
            'label': 'Bicicletas',
            'data': [112, 173, 283, 299, 181, 159, 245],
            'color': 'rgb(77 253 155)',
            'colorH': 'rgb(77 253 155 / 60%)',
            'dataHour': [247, 246, 232, 234, 175, 235, 208, 111, 140, 153, 221, 124, 260, 140, 291, 158, 110, 122, 136, 195, 105, 135, 202, 267]
        },
        'motorbikes': {
            'label': 'Motocicletas',
            'data': [2012, 1473, 1783, 1599, 1081, 1759, 2745],
            'color': 'rgb(0 171 60)',
            'colorH': 'rgb(0 171 60 / 60%)',
            'dataHour': [1498, 2624, 3630, 3571, 2529, 2708, 1416, 3818, 1930, 3164, 1306, 1925, 2248, 3315, 3896, 2564, 3004, 1040, 2050, 1069, 1356, 1047, 2890, 1949]
        }
    }

    places = [{
        'label': 'Praça Dante Alighieri - Caxias do Sul / RS',
        'value': 'p1'
    }]
    
    return render_template('index.html', title='Home', variables=variables, appName='NOME MANEIRO PRO APP', places=places)

if __name__ == '__main__':
    app.run()