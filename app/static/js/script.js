// function getRandomInt(min, max) {
//     min = Math.ceil(min);
//     max = Math.floor(max);
//     return Math.floor(Math.random() * (max - min + 1)) + min;
// }

// function generateRandomArray(length, min, max) {
//     return Array.from({ length }, () => getRandomInt(min, max));
// }

// const variables = {
//     "carros": {
//         "label": "Carros",
//         "data": [2012, 5473, 4783, 3599, 3081, 2759, 3745],
//         "color": 'rgb(77 83 253)',
//         "colorH": 'rgb(77 83 253 / 60%)',
//         "dataHour": generateRandomArray(24, 2000, 6000)
//     },
//     "buzes": {
//         "label": "Ônibus",
//         "data": [500, 573, 483, 399, 381, 259, 345],
//         "color": 'rgb(145 77 253)',
//         "colorH": 'rgb(145 77 253 / 60%)',
//         "dataHour": generateRandomArray(24, 200, 600)
//     },
//     "bikes": {
//         "label": "Bicicletas",
//         "data": [112, 173, 283, 299, 181, 159, 245],
//         "color": 'rgb(77 253 155)',
//         "colorH": 'rgb(77 253 155 / 60%)',
//         "dataHour": generateRandomArray(24, 100, 300)
//     },
//     "motorbikes": {
//         "label": "Motocicletas",
//         "data": [2012, 1473, 1783, 1599, 1081, 1759, 2745],
//         "color": 'rgb(0 171 60)',
//         "colorH": 'rgb(0 171 60 / 60%)',
//         "dataHour": generateRandomArray(24, 1000, 4000)
//     }
// }

function getRatio(value) {
    return value > 3000 ? 10 : value > 1000 ? 7 : 5
}

const days = [
    'Domingo',
    'Segunda-Feira',
    'Terça-Feira',
    'Quarta-Feira',
    'Quinta-Feira',
    'Sexta-Feira',
    'Sábado'
]
const hours = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];

luxon.Settings.defaultLocale = "pt-br";

function loadCharts(variables) {
    const chartVehiclesLine = new Chart(
        document.getElementById('chart-vehicles-line'),
        {
            type: 'line',
            data: {
                labels: days,
                datasets: [
                    {
                        label: variables.carros.label,
                        backgroundColor: variables.carros.color,
                        borderColor: variables.carros.color,
                        data: variables.carros.data,
                    },
                    {
                        label: variables.buzes.label,
                        backgroundColor: variables.buzes.color,
                        borderColor: variables.buzes.color,
                        data: variables.buzes.data,
                    },
                    {
                        label: variables.motorbikes.label,
                        backgroundColor: variables.motorbikes.color,
                        borderColor: variables.motorbikes.color,
                        data: variables.motorbikes.data,
                    },
                    {
                        label: variables.bikes.label,
                        backgroundColor: variables.bikes.color,
                        borderColor: variables.bikes.color,
                        data: variables.bikes.data,
                    }
                ]
              },
              options: {
                maintainAspectRatio: false,
                responsive: true,
              }
        }
      );
    
    const chartVehiclesDoughnut = new Chart(
        document.getElementById('chart-vehicles-doughnut'),
        {
            type: 'doughnut',
            data: {
                labels: Object.keys(variables).map(key => variables[key].label),
                datasets: [
                    {
                        label: 'Veículos',
                        data: Object.keys(variables).map(key => variables[key].data.reduce((acc, val) => acc + val, 0)),
                        backgroundColor: Object.keys(variables).map(key => variables[key].color),
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    }
                }
            }
        }
      );
    
    const chartVehiclesScatter = new Chart(
        document.getElementById('chart-vehicles-scatter'),{
        type: 'bubble',
        data: {
            datasets: Object.keys(variables).map(key => ({
                label: variables[key].label,
                data: variables[key].dataHour.map((value, index) => ({
                    x: hours[index],
                    y: value,
                    r: getRatio(value)
                })),
                borderColor: variables[key].color,
                backgroundColor: variables[key].colorH
            }))
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                x: {
                    type: "time",
                    time: {
                        unit: 'hour',
                        displayFormats: {
                            hour: 'HH:mm'
                        }
                    },
                    ticks:{
                        maxTicksLimit: 16,
                    }
                  }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(context) {
                            var label = `${context.raw.y || 0} ${context.dataset.label.toLowerCase()}`;
                            return label;
                        }
                    }
                }
            }
        }
    });

    return {
        chartVehiclesLine,
        chartVehiclesDoughnut,
        chartVehiclesScatter
    }
}

document.getElementById('darkmode').addEventListener('change', (event) => {
    event.target.checked ? document.body.classList.add("darkmode") : document.body.classList.remove("darkmode");
});

const modal = document.getElementById("modal");
const modalDescription = document.getElementById("modal-description");
const btnClose = document.getElementById("close")

btnClose.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    modalDescription.innerHTMl = "";
  }
}

document.querySelectorAll('.info').forEach(span => {
    span.addEventListener('click', (event) => {
        modalDescription.innerHTML = event.target.getAttribute("data-text");
        modal.style.display = "block";
    });
});